const { beforeEach } = require("mocha")

describe("Tickets", () => {
    beforeEach(() => {
        cy.HomePage()
    })

    it("Checando o Título", () => {
        cy.get("#app h1").should("contain", "TICKETBOX")
    })

    it("Escrevendo nos campos", () => {
        const userFields = {
            firstName: "Patrick",
            lastName: "Lima",
            email: "patrick.lima@outlook.com"
        }
        
        cy.FillUserFields(userFields)
    })

    it("Escolhendo quantidade de tickets", () => {
        cy.get("#ticket-quantity").select("2")
    })

    it("Como soube do evento?", () => {
        cy.get("#social-media").check()
    })
})